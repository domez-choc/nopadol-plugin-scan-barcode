/* global cordova:false */
/* globals window */

var exec = cordova.require('cordova/exec'),
    utils = cordova.require('cordova/utils');

var PLUGIN_NAME = 'NopadolPluginScanBarcode';

var template = {
    start: function(successCallback, errorCallback, message, forceAsync) {
        var action = 'start';

        if (forceAsync) {
            action += 'Async';
        }

        exec(successCallback, errorCallback, PLUGIN_NAME, action, [message]);
    }
};

module.exports = template;
